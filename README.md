# Package for bamUtil

Programs that perform operations on SAM/BAM files, all built into a single executable, bam.
[https://github.com/statgen/bamUtil]
Singularity container based on the recipe: Singularity.bamutil
Package installation using Miniconda3-4.7.12

```bash
sudo singularity build bamUtil.sif Singularity.bamutil
```
## Get image help:

```bash
singularity run-help bamUtil.sif
```
### Default runscript: bam
## Usage:
```bash
./bamUtil.sif --help
```
or:
```bash
singularity exec bamUtil.sif bam --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull bamUtil.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/bamutil/bamutil:latest

